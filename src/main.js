import Vue from 'vue'
import VueRouter from "vue-router";
import App from './App.vue'
// import BloodPressureTracker from "@/components/Pages/BloodPressureTracker";
// import DiabetesTracker from "@/components/Pages/DiabetesTracker";
import 'materialize-css';
import 'materialize-css/dist/css/materialize.css';
import HomePage from "@/components/Pages/HomePage";
import Login from "@/components/Pages/Login";
import SignUp from "@/components/Pages/SignUp";
import Profile from "@/components/Pages/Profile";

Vue.config.productionTip = false;

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  {path: '/', component: HomePage},
  {path: '/login', component: Login},
  {path: '/signup', component: SignUp},
  {path: '/home', component: HomePage},
  {path: '/profile', component: Profile}
];

Vue.use(VueRouter);
// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
});

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
new Vue({
  render: h => h(App),
  router
}).$mount('#app');

// Now the app has started!
